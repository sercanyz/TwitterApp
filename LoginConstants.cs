﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace TwitterApp.Constants
{
    public class LoginConstants
    {
        public IWebDriver driver { get; }
        public LoginConstants(IWebDriver driver) { driver = driver; }

        public string URL = "https://twitter.com/login";
        public string KullaniciAdi = "sercanyzz";
        public string Sifre = "Bimtas90.";

        public IWebElement username => driver.FindElement(By.XPath("//input[contains(@type,'text')]"));
        public IWebElement password => driver.FindElement(By.XPath("//input[contains(@type,'password')]"));
        public IWebElement loginBtn => driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/main[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[3]/div[1]/div[1]/span[1]/span[1]"));
    }
}
