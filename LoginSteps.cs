﻿using System;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using TwitterApp.Constants;
using OpenQA.Selenium.Chrome;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace TwitterApp
{
    [Binding, Scope(Feature ="LoginFeatures")]
    public class LoginSteps
    {

        LoginConstants loginConstants = null;

        IWebDriver driver = new ChromeDriver();


        [Given(@"Twitter Web Sitesini Ac")]
        public void GivenTwitterWebSitesiniAc()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://twitter.com/login");
            loginConstants = new LoginConstants(driver);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
        }

        [Given(@"Kullanici Adi Gir")]
        public void GivenKullaniciAdiGir()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            driver.FindElement(By.XPath("//input[contains(@type,'text')]")).SendKeys("sercanyzz");
            //loginConstants.username.SendKeys(loginConstants.KullaniciAdi);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

        }

        [When(@"Sifre Gir")]
        public void WhenSifreGir()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            driver.FindElement(By.XPath("//input[contains(@type,'password')]")).SendKeys("Bimtas90.");
            //loginConstants.password.SendKeys(loginConstants.Sifre);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
        }

        [Then(@"Log In Butonuna Bas")]
        public void ThenLogInButonunaBas()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/main[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[3]/div[1]/div[1]/span[1]/span[1]")).Click();
            //loginConstants.loginBtn.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
        }
    }
}
